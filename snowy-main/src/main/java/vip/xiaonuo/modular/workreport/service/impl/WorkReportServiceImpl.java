/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workreport.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import vip.xiaonuo.core.context.login.LoginContextHolder;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.task.entity.Task;
import vip.xiaonuo.modular.task.enums.TaskStatusEnum;
import vip.xiaonuo.modular.task.mapper.TaskMapper;
import vip.xiaonuo.modular.task.service.TaskService;
import vip.xiaonuo.modular.workorder.entity.WorkOrder;
import vip.xiaonuo.modular.workorder.enums.WorkOrderExceptionEnum;
import vip.xiaonuo.modular.workorder.enums.WorkOrderStatusEnum;
import vip.xiaonuo.modular.workorder.service.WorkOrderService;
import vip.xiaonuo.modular.workreport.entity.WorkReport;
import vip.xiaonuo.modular.workreport.enums.WorkReportApprovalEnum;
import vip.xiaonuo.modular.workreport.enums.WorkReportDistinctionEnum;
import vip.xiaonuo.modular.workreport.enums.WorkReportExceptionEnum;
import vip.xiaonuo.modular.workreport.mapper.WorkReportMapper;
import vip.xiaonuo.modular.workreport.param.WorkReportParam;
import vip.xiaonuo.modular.workreport.result.WorkReportResult;
import vip.xiaonuo.modular.workreport.service.WorkReportService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 报工表service接口实现类
 *
 * @author 楊楊
 * @date 2022-05-30 14:08:42
 */
@Service
public class WorkReportServiceImpl extends ServiceImpl<WorkReportMapper, WorkReport> implements WorkReportService {

    @Autowired
    WorkReportMapper workReportMapper;
    @Autowired
    TaskService taskService;
    @Autowired
    TaskMapper taskMapper;
    @Autowired
    WorkOrderService workOrderService;

    @Override
    public PageResult<WorkReportResult> page(WorkReportParam workReportParam) {
        QueryWrapper<WorkReportResult> queryWrapper = getWorkReportResultQueryWrapper(workReportParam);

        return new PageResult<>(this.baseMapper.page(PageFactory.defaultPage(), queryWrapper));
    }


    @Override
    public List<WorkReportResult> list(WorkReportParam workReportParam) {

        return this.baseMapper.workReportList(getWorkReportResultQueryWrapper(workReportParam));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(WorkReportParam workReportParam) {
        //如果数量为负或空抛出异常
        if (ObjectUtil.isEmpty(workReportParam.getBadNum()) || ObjectUtil.isEmpty(workReportParam.getGoodNum()) || ObjectUtil.isEmpty(workReportParam.getReportNum()) || workReportParam.getReportNum() < 0 || workReportParam.getGoodNum() < 0 || workReportParam.getBadNum() < 0) {
            throw new ServiceException(WorkReportExceptionEnum.NOT_ALLOWED);
        }
        //如果报工人员为空默认登录用户
        if (ObjectUtil.isEmpty(workReportParam.getProductionUser())) {
            workReportParam.setProductionUser(LoginContextHolder.me().getSysLoginUserId());
        }
        //如果报工状态为空默认执行中
//        if(ObjectUtil.isEmpty(workReportParam.getStepStatus())){
//            workReportParam.setStepStatus(WorkReportStatusEnum.EXECUTION.getCode());
//        }
//        String[] timeList = null;
        if (ObjectUtil.isNotEmpty(workReportParam.getStartEndTime())) {
            String[] timeList = workReportParam.getStartEndTime().split(";");
            if (ObjectUtil.isNotEmpty(timeList) && timeList.length >= 2 && timeList[0].compareTo(timeList[1])<0) {
                workReportParam.setStartTime(timeList[0]);
                workReportParam.setEndTime(timeList[1]);
            }else{
                throw  new ServiceException(WorkReportExceptionEnum.REPORT_START_END_TIME);
            }
        }
        //添加报工
        WorkReport workReport = new WorkReport();
        BeanUtil.copyProperties(workReportParam, workReport);
        workReport.setDistinction(WorkReportDistinctionEnum.WORK_REPORT_ADD.getCode());
        workReport.setApproval(WorkReportApprovalEnum.APPROVAL_NO.getCode());
        this.save(workReport);
        //判断工单是否存在
        WorkOrder workOrder = workOrderService.getById(workReportParam.getWorkOrderId());
        if(ObjectUtil.isNotEmpty(workOrder)){
            //工单状态不存在
            if(ObjectUtil.isEmpty(workOrder.getStatus())){
                throw new ServiceException(WorkReportExceptionEnum.NULL_ORDER_STATUS);
            }
            //工单状态未开始
            if(WorkOrderStatusEnum.NOT_START.getCode().equals(workOrder.getStatus())){
                throw new ServiceException(WorkReportExceptionEnum.NOT_START_ORDER);
            }
            //工单状态已结束
            if(WorkOrderStatusEnum.FINISH.getCode().equals(workOrder.getStatus())){
                throw new ServiceException(WorkReportExceptionEnum.END_ORDER);
            }
            //工单状态已取消
            if (WorkOrderStatusEnum.CANCELED.getCode().equals(workOrder.getStatus())){
                throw new ServiceException(WorkReportExceptionEnum.CANCELED_ORDER);
            }
        }
        //判断任务是否存在;存在修改任务良品数和不良品数
        Task task = taskService.getById(workReportParam.getTaskId());
        if (ObjectUtil.isNotEmpty(task)) {
            //任务状态为空抛出异常
            if (ObjectUtil.isEmpty(task.getStatus())) {
                throw new ServiceException(WorkReportExceptionEnum.NULL_TASK_STATUS);
            }
            //任务状态为已结束抛出异常
            if (TaskStatusEnum.FINISH.getCode().equals(task.getStatus())) {
                throw new ServiceException(WorkReportExceptionEnum.END_TASK);
            }
            //任务状态为未开始抛出异常
            if (task.getStatus().equals(TaskStatusEnum.NOT_START.getCode())) {
                throw new ServiceException(WorkReportExceptionEnum.NOT_START_TASK);
            }


            /*==============================任务存在Start===================================*/
            /*==============================计算良品数和不良品数=========================================*/
            int goodNum = 0;
            int badNum = 0;
//                //最小开始时间
//                String mixStartTime = null ;
//                //最大结束时间
//                String maxEndTime = null  ;
            QueryWrapper<WorkReport> workReportQueryWrapper = new QueryWrapper();
            workReportQueryWrapper.lambda().eq(WorkReport::getTaskId, workReportParam.getTaskId());
            //根据任务Id查询所有报工
            List<WorkReport> workReportList = workReportMapper.selectList(workReportQueryWrapper);
            //根据报工计算当前该任务的良品数和不良品数
            if (ObjectUtil.isNotEmpty(workReportList)) {
                for (WorkReport item : workReportList) {
                    goodNum += item.getGoodNum();
                    badNum += item.getBadNum();
                }
//                    //获取最小时间
//                    mixStartTime = workReportList.get(0).getStartTime().toString();
//                    if(timeList[0].compareTo(mixStartTime)<0){
//                        mixStartTime = timeList[0];
//                    }
//                    //获取最最大时间
//                    maxEndTime = workReportList.get(workReportList.size()-1).getStartTime().toString();
//                    if(timeList[1].compareTo(maxEndTime)>0){
//                        maxEndTime = timeList[1];
//                    }
            }
            /*==============================计算良品数和不良品数=========================================*/

            previousTask(workReportParam, task, goodNum);

            //判断上一个任务是否完成
//                            if(ObjectUtil.isNotEmpty(tasks.get(i - 1).getStatus())&&!tasks.get(i - 1).getStatus().equals(TaskStatusEnum.FINISH.getCode())) {
//                                throw new ServiceException(WorkReportExceptionEnum.NOT_COMPLETED);
//                            }
            //修改任务良品数和不良品数
            task.setGoodNum(goodNum);
            task.setBadNum(badNum);
//                            .setFactStaTime(mixStartTime)
//                            .setFactStaTime(null);
            //报工已结束,传递结束时间
//                    if (workReportParam.getStepStatus().equals(WorkReportStatusEnum.FINISH.getCode())) {
//                        taskParam.setFactEndTime(maxEndTime);
//                    }
            taskMapper.updateById(task);

        }
    }


    /**
     * 可删除
     *
     * @param
     * @return
     */
//    private Integer TaskStatus(WorkReportParam workReportParam) {
//        if (ObjectUtil.isNotEmpty(workReportParam.getStepStatus())) {
//            if (workReportParam.getStepStatus() .equals(WorkReportStatusEnum.EXECUTION.getCode())) {
//                return WorkReportStatusEnum.EXECUTION.getCode();
//            } else if (workReportParam.getStepStatus() .equals(WorkReportStatusEnum.FINISH.getCode())) {
//                return WorkReportStatusEnum.FINISH.getCode();
//            }
//        }
//        //报工未开始抛出异常
//            throw new ServiceException(WorkReportExceptionEnum.NOT_START_REPORT);
//
//
//    }
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<WorkReportParam> workReportParamList) {
//报工Id
        List<Long> workReportIdList = new ArrayList<>();
        //获取报工Id
        workReportParamList.forEach(workReportParam -> {
            workReportIdList.add(workReportParam.getId());
        });
        //根据报工Id查询报工
        QueryWrapper<WorkReport> workReportQueryWrapper = new QueryWrapper<>();
        workReportQueryWrapper.lambda().in(WorkReport::getId, workReportIdList);
        List<WorkReport> workReportList = this.baseMapper.selectList(workReportQueryWrapper);


        //删除报工
        workReportParamList.forEach(workReportParam -> {
            this.removeById(workReportParam.getId());
        });

        //获取报工的任务Id
        List<Long> taskIdsList = new ArrayList<>();
        workReportList.forEach(workReportParam -> {
            taskIdsList.add(workReportParam.getTaskId());
        });
        //任务Id去重
        List<Long> taskIdList = new ArrayList<>();
        taskIdList.addAll(taskIdsList.stream().distinct().collect(Collectors.toList()));

//        根据任务Id计算 剩余报工的良品数,不良品数
        QueryWrapper<WorkReport> workReportNumQueryWrapper = new QueryWrapper<>();
        workReportNumQueryWrapper.select("sum(good_num) as goodNum", "sum(bad_num) as badNum", "task_id");
        for (int i = 0; i < taskIdList.size(); i++) {
            workReportNumQueryWrapper.lambda().eq(WorkReport::getTaskId, taskIdList.get(i));
            if (i != taskIdList.size() - 1) {
                workReportNumQueryWrapper.or();
            }
        }
        workReportNumQueryWrapper.lambda().groupBy(WorkReport::getTaskId);
        List<WorkReport> tasks = this.baseMapper.selectList(workReportNumQueryWrapper);


        //查询所有任务
        List<Task> allTask = taskService.list();
        List<Task> taskList = new ArrayList<>();
        //如果当前任务下报工全部删除,tasks为空, 根据任务Id修改
        taskIdList.forEach(taskId -> {
            allTask.forEach(task -> {
                if (task.getId().equals(taskId)) {
                    task.setGoodNum(0);
                    task.setBadNum(0);
                    taskList.add(task);
                }
            });
        });
        if (ObjectUtil.isNotEmpty(tasks)) {
            tasks.forEach(workReport -> {
                allTask.forEach(task -> {
                    if (task.getId().equals(workReport.getTaskId())) {
                        task.setGoodNum(workReport.getGoodNum());
                        task.setBadNum(workReport.getBadNum());
                        taskList.add(task);
                    }
                });
            });
        }
        taskService.updateBatchById(taskList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(WorkReportParam workReportParam) {
        WorkReport workReport = this.queryWorkReport(workReportParam);
        //如果数量为负抛出异常
        if (workReportParam.getReportNum() < 0 || workReportParam.getGoodNum() < 0 || workReportParam.getBadNum() < 0) {
            throw new ServiceException(WorkReportExceptionEnum.NOT_ALLOWED);
        }
        //如果报工人员为空默认登录用户
        if (ObjectUtil.isEmpty(workReportParam.getProductionUser())) {
            workReportParam.setProductionUser(LoginContextHolder.me().getSysLoginUserId());
        }
        //对开始结束时间进行处理
        if (ObjectUtil.isNotEmpty(workReportParam.getStartEndTime())) {
            String[] timeList = workReportParam.getStartEndTime().split(";");
            if (ObjectUtil.isNotEmpty(timeList) && timeList.length >= 2 && timeList[0].compareTo(timeList[1])<0) {
                workReportParam.setStartTime(timeList[0]);
                workReportParam.setEndTime(timeList[1]);
            }else {
                throw new ServiceException(WorkReportExceptionEnum.REPORT_START_END_TIME);
            }
        }
        //修改报工
        BeanUtil.copyProperties(workReportParam, workReport);
        this.updateById(workReport);
        //判断任务是否存在
        Task task = taskService.getById(workReportParam.getTaskId());
        if (ObjectUtil.isNotEmpty(task)) {
            //任务状态为空抛出异常
            if (ObjectUtil.isEmpty(task.getStatus())) {
                throw new ServiceException(WorkReportExceptionEnum.NULL_TASK_STATUS);
            }
            //任务状态为已结束抛出异常
            if (TaskStatusEnum.FINISH.getCode().equals(task.getStatus())) {
                throw new ServiceException(WorkReportExceptionEnum.END_TASK);
            }
            //任务状态为未开始抛出异常
            if (task.getStatus().equals(TaskStatusEnum.NOT_START.getCode())) {
                throw new ServiceException(WorkReportExceptionEnum.NOT_START_TASK);
            }

            /*==============================计算良品数和不良品数=========================================*/
            int goodNum = 0;
            int badNum = 0;
            QueryWrapper<WorkReport> workReportQueryWrapper = new QueryWrapper();
            workReportQueryWrapper.lambda().eq(WorkReport::getTaskId, workReportParam.getTaskId());
            List<WorkReport> workReportList = workReportMapper.selectList(workReportQueryWrapper);
            if (ObjectUtil.isNotEmpty(workReportList)) {
                for (WorkReport item : workReportList
                ) {
                    //获取数据库当前报工数量
                    goodNum += item.getGoodNum();
                    badNum += item.getBadNum();
                }
            }
            /*==============================计算良品数和不良品数=========================================*/
            //判断良品数与上一个任务的良品数的关系,如果大于抛出异常,第一个任务无需判断
            previousTask(workReportParam, task, goodNum);
            //判断良品数与下一个任务的良品数的关系,如果小于抛出异常,最后一个任务无需判断
            nextTask(workReportParam, task, goodNum);

            task.setGoodNum(goodNum);
            task.setBadNum(badNum);
            this.taskMapper.updateById(task);

        }

    }


    @Override
    public WorkReport detail(WorkReportParam workReportParam) {
        return this.queryWorkReport(workReportParam);
    }
    //审批报工表
    @Override
    public void approval(WorkReportParam workReportParam) {
        if(ObjectUtil.isNotEmpty(workReportParam)){
            if(ObjectUtil.isNotEmpty(workReportParam.getApproval())&&workReportParam.getApproval().equals(1)){
                workReportParam.setApproval(WorkReportApprovalEnum.APPROVAL_NO.getCode());
            } else
            {
                workReportParam.setApproval(WorkReportApprovalEnum.APPROVAL_YES.getCode());
            }
            WorkReport workReport = new WorkReport();
            BeanUtil.copyProperties(workReportParam,workReport);
            this.baseMapper.updateById(workReport);
        }
    }

    /**
     * 获取报工表
     *
     * @author 楊楊
     * @date 2022-05-30 14:08:42
     */
    private WorkReport queryWorkReport(WorkReportParam workReportParam) {
        WorkReport workReport = this.getById(workReportParam.getId());
        if (ObjectUtil.isNull(workReport)) {
            throw new ServiceException(WorkReportExceptionEnum.NOT_EXIST);
        }
        return workReport;
    }

    @Override
    public void export(WorkReportParam workReportParam) {
        List<WorkReportResult> list = this.list(workReportParam);
        PoiUtil.exportExcelWithStream("SnowyWorkReport.xls", WorkReport.class, list);
    }

    private QueryWrapper<WorkReportResult> getWorkReportResultQueryWrapper(WorkReportParam workReportParam) {
        QueryWrapper<WorkReportResult> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotEmpty(workReportParam)) {

            // 根据结束时间 查询
            if (ObjectUtil.isNotEmpty(workReportParam.getEndTime())) {

                Date endTime = new Date();
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    endTime = sdf.parse(workReportParam.getEndTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(endTime);
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                endTime = calendar.getTime();

                String formatEnd = new SimpleDateFormat("yyyy-MM-dd").format(endTime);

                queryWrapper.lambda().le(WorkReportResult::getEndTime, formatEnd);
            }
            // 根据开始时间 查询
            if (ObjectUtil.isNotEmpty(workReportParam.getStartTime())) {

//                Date  startTime = new Date();
//                SimpleDateFormat sdf =   new SimpleDateFormat( "yyyy-MM-dd" );
//                try {
//                    startTime = sdf.parse(workReportParam.getStartTime());
//                    Calendar calendar = new GregorianCalendar();
//                    calendar.setTime(startTime);
//                    calendar.add(Calendar.DATE,-1);
//                    startTime = calendar.getTime();
//                    String formatStr = new SimpleDateFormat("yyyy-MM-dd").format(startTime);
//
//                    System.out.println(formatStr+"--++");
//                    queryWrapper.lambda().ge(WorkReportResult::getStartTime,formatStr);
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
                queryWrapper.lambda().ge(WorkReportResult::getStartTime, workReportParam.getStartTime());

            }
            // 根据工单id 查询
            if (ObjectUtil.isNotEmpty(workReportParam.getWorkOrderId())) {
                queryWrapper.eq("r.work_order_id", workReportParam.getWorkOrderId());
            }
            // 根据生产人员 查询
            if (ObjectUtil.isNotEmpty(workReportParam.getProductionUser())) {
                queryWrapper.lambda().eq(WorkReportResult::getProductionUser, workReportParam.getProductionUser());
            }

            // 根据任务id 查询
            if (ObjectUtil.isNotEmpty(workReportParam.getTaskId())) {
                queryWrapper.lambda().eq(WorkReportResult::getTaskId, workReportParam.getTaskId());
            }
            //           根据工序状态 查询
            if (ObjectUtil.isNotEmpty(workReportParam.getStatus())) {
                queryWrapper.eq("t.status", workReportParam.getStatus());
            }
            //           根据工序Id 查询
            if (ObjectUtil.isNotEmpty(workReportParam.getStepId())) {
                queryWrapper.eq("t.work_step_id", workReportParam.getStepId());
            }
            // 根据不良品项 查询
//            if (ObjectUtil.isNotEmpty(workReportParam.getMulBadItem())) {
//                String[] mulBadItemlist=workReportParam.getMulBadItem().split(";");
//                queryWrapper.nested(item->{
//                            for ( String mulBadItem: mulBadItemlist)
//                            {
//                                item.lambda().apply("instr(concat(';',mul_bad_item,';'),concat(';',{0},';'))",mulBadItem).or();
//                            }
//                        }
//                );
//            }
        }
        queryWrapper.orderByDesc("r.create_time");
        return queryWrapper;
    }

    private void previousTask(WorkReportParam workReportParam, Task task, int goodNum) {

        if (ObjectUtil.isEmpty(task.getSortNum())) {
            throw new ServiceException(WorkReportExceptionEnum.NOT_TASK_SORNUMS);
        }
        //获取任务顺序
        QueryWrapper<Task> taskPreviousQueryWrapper = new QueryWrapper<>();
        taskPreviousQueryWrapper.select("max(sort_num) as sortNum");
        taskPreviousQueryWrapper.lambda().eq(Task::getWorkOrderId, workReportParam.getWorkOrderId()).lt(Task::getSortNum, task.getSortNum());
        List<Task> taskPreviousSortNum = taskMapper.selectList(taskPreviousQueryWrapper);

        //如果为空,该任务为第一个任务.如果不为空,获取上一个任务
        if (ObjectUtil.isNotEmpty(taskPreviousSortNum) && taskPreviousSortNum.get(0) != null) {
            if (ObjectUtil.isEmpty(taskPreviousSortNum.get(0).getSortNum())) {
                throw new ServiceException(WorkReportExceptionEnum.NOT_TASK_SORNUMS);
            }
            if (taskPreviousSortNum.size() > 1) {
                throw new ServiceException(WorkReportExceptionEnum.MORE_TASK_SORNUMS);
            }
            QueryWrapper<Task> taskQueryWrapper = new QueryWrapper<>();
            taskQueryWrapper.lambda().eq(Task::getSortNum, taskPreviousSortNum.get(0).getSortNum()).eq(Task::getWorkOrderId, workReportParam.getWorkOrderId());
            List<Task> taskPrevious = taskMapper.selectList(taskQueryWrapper);
            //如果tasks为空,该任务为第一个任务
            //如果该任务良品数大于上次任务良品数抛出异常
            if (ObjectUtil.isEmpty(taskPrevious.get(0).getGoodNum())) {
                taskPrevious.get(0).setGoodNum(0);
            }
            if (ObjectUtil.isNotEmpty(taskPrevious) && ObjectUtil.isNotEmpty(workReportParam.getGoodNum()) && goodNum > taskPrevious.get(0).getGoodNum()) {
                throw new ServiceException(WorkReportExceptionEnum.MORE_GOODNUM);
            }

        }
    }

    private void nextTask(WorkReportParam workReportParam, Task task, int goodNum) {
        if (ObjectUtil.isEmpty(task.getSortNum())) {
            throw new ServiceException(WorkReportExceptionEnum.NOT_TASK_SORNUMS);
        }
        //获取任务顺序
        QueryWrapper<Task> taskNextQueryWrapper = new QueryWrapper<>();
        taskNextQueryWrapper.select("min(sort_num) as sortNum");
        taskNextQueryWrapper.lambda().eq(Task::getWorkOrderId, workReportParam.getWorkOrderId()).gt(Task::getSortNum, task.getSortNum());
        List<Task> taskNextSortNum = taskMapper.selectList(taskNextQueryWrapper);
        //如果为空,该任务为最后一个任务.如果不为空,获取下一个任务
        if (ObjectUtil.isNotEmpty(taskNextSortNum) && taskNextSortNum.get(0) != null) {
            if (ObjectUtil.isEmpty(taskNextSortNum.get(0).getSortNum())) {
                throw new ServiceException(WorkReportExceptionEnum.NOT_TASK_SORNUMS);
            }
            if (taskNextSortNum.size() > 1) {
                throw new ServiceException(WorkReportExceptionEnum.MORE_TASK_SORNUMS);
            }
            QueryWrapper<Task> taskQueryWrapper = new QueryWrapper<>();
            taskQueryWrapper.lambda().eq(Task::getSortNum, taskNextSortNum.get(0).getSortNum()).eq(Task::getWorkOrderId, workReportParam.getWorkOrderId());
            List<Task> taskNext = taskMapper.selectList(taskQueryWrapper);
            //如果tasks为空,该任务为第一个任务
            //如果该任务良品数小于下次任务良品数抛出异常
            if (ObjectUtil.isEmpty(taskNext.get(0).getGoodNum())) {
                taskNext.get(0).setGoodNum(0);
            }
            if (ObjectUtil.isNotEmpty(taskNext) && ObjectUtil.isNotEmpty(workReportParam.getGoodNum()) && goodNum < taskNext.get(0).getGoodNum()) {
                throw new ServiceException(WorkReportExceptionEnum.MIN_GOODNUM);
            }
        }
    }

}
