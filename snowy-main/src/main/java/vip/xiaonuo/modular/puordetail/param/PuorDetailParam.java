/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.puordetail.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import java.util.*;

/**
* 采购细明参数类
 *
 * @author jiaxin
 * @date 2022-07-27 15:57:25
*/
@Data
public class PuorDetailParam extends BaseParam {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;

    /**
     * 采购订单编号
     */
    @NotBlank(message = "采购订单编号不能为空，请检查purchaseOrderId参数", groups = {add.class, edit.class})
    private String purchaseOrder;

    /**
     * 采购明细编号
     */
    @NotBlank(message = "采购明细编号不能为空，请检查code参数", groups = {add.class, edit.class})
    private String code;

    /**
     * 产品名称
     */
    @NotBlank(message = "产品名称不能为空，请检查proId参数", groups = {add.class, edit.class})
    private String proName;

    /**
     * 数量
     */
    @NotNull(message = "数量不能为空，请检查purchaseNum参数", groups = {add.class, edit.class})
    private Integer purchaseNum;

    /**
     * 单价
     */
    @NotNull(message = "单价不能为空，请检查unitPrice参数", groups = {add.class, edit.class})
    private Double unitPrice;

    /**
     * 总价
     */
    @NotNull(message = "总价不能为空，请检查totalPrice参数", groups = {add.class, edit.class})
    private Double totalPrice;

    /**
     * 到货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private String arrivalTime;
    /**
     * 采购订单来源
     */
    private Long orderSource;
//    /**
//     * 产品名字
//     */
//    private String proName;

    /**
     * 采购订单ID数组
     */
    private String orderIds;

    /**
     * 产品ID数组
     */
    private String proIds;
    /**
     * 产品编码
     */
    private String proCode;


}
