import { axios } from '@/utils/request'

/**
 * 查询物料清单
 *
 * @author wz
 * @date 2022-08-11 09:15:20
 */
export function bomPage (parameter) {
  return axios({
    url: '/bom/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 物料清单列表
 *
 * @author wz
 * @date 2022-08-11 09:15:20
 */
export function bomList (parameter) {
  return axios({
    url: '/bom/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加物料清单
 *
 * @author wz
 * @date 2022-08-11 09:15:20
 */
export function bomAdd (parameter) {
  return axios({
    url: '/bom/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑物料清单
 *
 * @author wz
 * @date 2022-08-11 09:15:20
 */
export function bomEdit (parameter) {
  return axios({
    url: '/bom/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除物料清单
 *
 * @author wz
 * @date 2022-08-11 09:15:20
 */
export function bomDelete (parameter) {
  return axios({
    url: '/bom/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出物料清单
 *
 * @author wz
 * @date 2022-08-11 09:15:20
 */
export function bomExport (parameter) {
  return axios({
    url: '/bom/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 根据父项ID查找子项ID
 *
 * @author wz
 * @date 2022-08-30 13:41:34
 */
export function bomSonList (parameter) {
  return axios({
    url: '/bom/sonList',
    method: 'get',
    params: parameter
  })
}
