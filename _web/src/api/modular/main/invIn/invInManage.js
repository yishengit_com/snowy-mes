import { axios } from '@/utils/request'

/**
 * 查询入库单
 *
 * @author wz
 * @date 2022-06-06 15:49:08
 */
export function invInPage (parameter) {
  return axios({
    url: '/invIn/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 入库单列表
 *
 * @author wz
 * @date 2022-06-06 15:49:08
 */
export function invInList (parameter) {
  return axios({
    url: '/invIn/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加入库单
 *
 * @author wz
 * @date 2022-06-06 15:49:08
 */
export function invInAdd (parameter) {
  return axios({
    url: '/invIn/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑入库单
 *
 * @author wz
 * @date 2022-06-06 15:49:08
 */
export function invInEdit (parameter) {
  return axios({
    url: '/invIn/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除入库单
 *
 * @author wz
 * @date 2022-06-06 15:49:08
 */
export function invInDelete (parameter) {
  return axios({
    url: '/invIn/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出入库单
 *
 * @author wz
 * @date 2022-06-06 15:49:08
 */
export function invInExport (parameter) {
  return axios({
    url: '/invIn/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
