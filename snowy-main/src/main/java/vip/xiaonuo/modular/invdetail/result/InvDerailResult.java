package vip.xiaonuo.modular.invdetail.result;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import vip.xiaonuo.core.pojo.base.param.BaseParam;
import vip.xiaonuo.modular.invdetail.entity.InvDetail;

import javax.validation.constraints.NotNull;

@Data
public class InvDerailResult extends InvDetail {
    /**
     * 产品名称
     */
    private String proName;
    /**
     * 库单编码
     */
    private String code;
    /**
     * 产品单位
     */
    private String unit;
    /**
     * 出入库时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private String time;
    /**
     * 来源
     */
    private int category;

    /**
     * 出库类型
     */
    private String outType;
    /**
     * 入库类型
     */
    private String inType;
    /**
     * 产品编码
     */
    private String proCode;


}
