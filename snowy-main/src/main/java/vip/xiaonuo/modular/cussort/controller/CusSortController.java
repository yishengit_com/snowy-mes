/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.cussort.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.DataScope;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.cussort.param.CusSortParam;
import vip.xiaonuo.modular.cussort.service.CusSortService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 客户分类控制器
 *
 * @author czw
 * @date 2022-07-20 10:24:38
 */
@RestController
public class CusSortController {

    @Resource
    private CusSortService cusSortService;

    /**
     * 查询客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    @Permission
    @GetMapping("/cusSort/page")
    @BusinessLog(title = "客户分类_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(CusSortParam cusSortParam) {
        return new SuccessResponseData(cusSortService.page(cusSortParam));
    }

    /**
     * 添加客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    @Permission
    @PostMapping("/cusSort/add")
    @BusinessLog(title = "客户分类_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(CusSortParam.add.class) CusSortParam cusSortParam) {
            cusSortService.add(cusSortParam);
        return new SuccessResponseData();
    }

    /**
     * 删除客户分类，可批量删除
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    @Permission
    @PostMapping("/cusSort/delete")
    @BusinessLog(title = "客户分类_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(CusSortParam.delete.class) List<CusSortParam> cusSortParamList) {
            cusSortService.delete(cusSortParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    @Permission
    @PostMapping("/cusSort/edit")
    @BusinessLog(title = "客户分类_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(CusSortParam.edit.class) CusSortParam cusSortParam) {
            cusSortService.edit(cusSortParam);
        return new SuccessResponseData();
    }

    /**
     * 查看客户分类
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    @Permission
    @GetMapping("/cusSort/detail")
    @BusinessLog(title = "客户分类_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(CusSortParam.detail.class) CusSortParam cusSortParam) {
        return new SuccessResponseData(cusSortService.detail(cusSortParam));
    }

    /**
     * 客户分类列表
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    @Permission
    @GetMapping("/cusSort/list")
    @BusinessLog(title = "客户分类_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(CusSortParam cusSortParam) {
        return new SuccessResponseData(cusSortService.list(cusSortParam));
    }

    /**
     * 导出系统用户
     *
     * @author czw
     * @date 2022-07-20 10:24:38
     */
    @Permission
    @GetMapping("/cusSort/export")
    @BusinessLog(title = "客户分类_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(CusSortParam cusSortParam) {
        cusSortService.export(cusSortParam);
    }

    /**
     * 获取组织机构树
     *
     * @author czw
     * @date 2020/7/21 11:55
     */
    @Permission
    @DataScope
    @GetMapping("/cusSort/tree")
    @BusinessLog(title = "客户类型树", opType = LogAnnotionOpTypeEnum.TREE)
    public ResponseData tree(CusSortParam cusSortParam) {
        return new SuccessResponseData(cusSortService.tree(cusSortParam));
    }

}