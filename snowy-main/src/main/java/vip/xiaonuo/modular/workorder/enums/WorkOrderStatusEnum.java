package vip.xiaonuo.modular.workorder.enums;

import lombok.Getter;

/**
 * @author 87761
 */
@Getter
public enum WorkOrderStatusEnum {
    /**
     * 未开始
     */
    NOT_START(-1,"未开始"),
    /**
     * 执行中
     */
    EXECUTION(0,"执行中"),
    /**
     * 已结束
     */
    FINISH(1,"已结束"),
    /**
     * 已取消
     */
    CANCELED(-2,"已取消"),

    /**
     * 撤销(动作)(数据库中无该状态)
     */
    revoke_action(-3, "撤销(动作)"),
    ;



    private final Integer code;

    private final String message;

    WorkOrderStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
