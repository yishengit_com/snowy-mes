/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.protype.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.node.AntdBaseTreeNode;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.protype.entity.ProType;
import vip.xiaonuo.modular.protype.param.ProTypeParam;
import java.util.List;

/**
 * 产品类型表service接口
 *
 * @author lixingda
 * @date 2022-05-20 14:01:44
 */
public interface ProTypeService extends IService<ProType> {

    /**
     * 查询产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    PageResult<ProType> page(ProTypeParam proTypeParam);

    /**
     * 产品类型表列表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    List<ProType> list(ProTypeParam proTypeParam);

    /**
     * 添加产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    void add(ProTypeParam proTypeParam);

    /**
     * 删除产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    void delete(List<ProTypeParam> proTypeParamList);

    /**
     * 编辑产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
    void edit(ProTypeParam proTypeParam);

    /**
     * 查看产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
     ProType detail(ProTypeParam proTypeParam);

    /**
     * 导出产品类型表
     *
     * @author lixingda
     * @date 2022-05-20 14:01:44
     */
     void export(ProTypeParam proTypeParam);

    /**
     * 获取系统组织机构树
     *
     * @param proTypeParam 查询参数
     * @return 产品类型树
     * @author lixingda
     * @date 2022/5/20 16:27
     */
    List<AntdBaseTreeNode> tree(ProTypeParam proTypeParam);

    /**
     * 根据节点id获取所有子节点id集合
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:31
     */
     List<Long> getChildIdListById(Long id);
}
